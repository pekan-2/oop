<?php 
    require ("frog.php");
    require ("ape.php");

    $sheep = new Animal("shaun");

    echo "Name : ".$sheep->name."<br>"; // "shaun"
    echo "Legs : ".$sheep->legs."<br>"; // 4
    echo "Cold blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->name."<br>"; // "shaun"
    echo "Legs : ".$sungokong->legs."<br>"; // 4
    echo "Cold blooded : ".$sungokong->cold_blooded."<br> Yell : "; // "no"
    $sungokong->yell();// "Auooo"
    echo "<br><br>"; 

    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name."<br>"; // "shaun"
    echo "Legs : ".$kodok->legs."<br>"; // 4
    echo "Cold blooded : ".$kodok->cold_blooded."<br> Jump : "; // "no"
    $kodok->jump() ; // "hop hop"
?>